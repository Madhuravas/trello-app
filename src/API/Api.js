import axios from "axios";

const ApiKey = "dc336cbd741eb8db35bfe2dd78eff829"
const ApiToken = "a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02"

export const getTrelloBoardsData =()=>{
    return axios
    .get(`https://api.trello.com/1/boards/633d0e9925c0f6019de8ea6e/lists?key=${ApiKey}&token=${ApiToken}`)
    
}

export const getTrelloListCards = (id) =>{
    return axios
    .get(`https://api.trello.com/1/lists/${id}/cards?key=${ApiKey}&token=${ApiToken}`)
    
}

export const getTrelloCheckLists = (cardId) =>{
    return axios
    .get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${ApiKey}&token=${ApiToken}`)
    .then(res =>res.data)
}

export const getTrelloCheckItems = (cardId) =>{
    return axios
    .get(`https://api.trello.com/1/checklists/${cardId}/checkItems?key=${ApiKey}&token=${ApiToken}`)
    .then(res => res.data)
}


// export const getTrelloCardsData = () =>{
//     return axios
//     .get(`https://api.trello.com/1/cards/633db2289965f000de192dd1?key=${ApiKey}&token=${ApiToken}`)
// }