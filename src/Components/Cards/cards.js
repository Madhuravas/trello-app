import { Component } from "react"

import * as TrelloData from "../../API/Api"
import { v4 as uuidv4 } from 'uuid';

import "./cards.css"
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';

class Cards extends Component {
    state = { cardsData: [], enterCardInputIsShow: false,addCardInput: "" }

    async componentDidMount() {

        const { id } = this.props
        await TrelloData.getTrelloListCards(id)
            .then((trelloBoardsData) => {
                this.setState({ cardsData: trelloBoardsData.data, isError: false })
            }).catch((err) => {
                this.setState({ isError: true })
            })
    }

    addCardItem = () => {
        const { addCardInput ,cardsData} = this.state
        const { id } = this.props

        fetch(`https://api.trello.com/1/cards?idList=${id}&name=${addCardInput}&key=dc336cbd741eb8db35bfe2dd78eff829&token=a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json'
            }
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));

        this.setState({cardsData: cardsData.concat({name:addCardInput,id:uuidv4()})})
        this.setState({ addCardInput: "" })
    }
    
    onAddCardInput = (event) => {
        this.setState({ addCardInput: event.target.value })
    }
    
    closeCardTitleInput = () => {
        this.setState({ enterCardInputIsShow: false })
    }

    openCardTitleInput = () => {
        this.setState({ enterCardInputIsShow: true })
    }

    enterCardText = () => {
        const { enterCardInputIsShow, addCardInput } = this.state
        if (enterCardInputIsShow) {
            return (
                <div>
                    <textarea value={addCardInput} onChange={this.onAddCardInput} className="text-area-input" placeholder="Enter a tilte for this card.." rows={4} cols={30} ></textarea>
                    <div className="add-close-buttons">
                        <button onClick={this.addCardItem} className="add-button-card">Add card</button>
                        <button onClick={this.closeCardTitleInput} className="add-card-close"><CloseIcon /></button>
                    </div>
                </div>
            )
        }
        return (
            <button onClick={this.openCardTitleInput} className="add-card-button">
                <AddIcon />
                Add a card
            </button>

        )
    }

    render() {
        const { cardsData } = this.state
        return (
            <>
                {cardsData.map(item => {
                    const showPopupPage = () => {
                        this.props.isPopupshows(item.id, item.name)
                    }
                    return (<div onClick={showPopupPage} key={item.id} className="card-container">
                        <h5 className="card-name">{item.name}</h5>
                    </div>

                    )
                })}
                {this.enterCardText()}
            </>)
    }
}


export default Cards