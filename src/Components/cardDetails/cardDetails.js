import { Component } from "react"

import CloseIcon from '@mui/icons-material/Close';
import CheckItems from "../CheckItems/checkItems";


import * as TrelloData from "../../API/Api"
import { v4 as uuidv4 } from 'uuid';

import "./cardDetails.css"

class CardDetails extends Component {
    state = { cardChecklist: [], checkListInputShow: false, checklistInput: "" }

    async componentDidMount() {
        const { cardId } = this.props

        await TrelloData.getTrelloCheckLists(cardId)
            .then(checkList => {
                this.setState({ cardChecklist: checkList })
            }).catch((err) => {
                console.log(err.message)
            })
    }

    closePopup = () => {
        const { closePopup } = this.props
        closePopup()
    }

    showChecklistAdd = () => {
        this.setState({ checkListInputShow: true })
    }

    closeUserInput = () => {
        this.setState({ checkListInputShow: false })
    }

    newCheckList = (event) => {
        this.setState({ checklistInput: event.target.value })
    }

    addCheckListInput = () => {
        const { checklistInput,cardChecklist } = this.state
        const { cardId } = this.props

        fetch(`https://api.trello.com/1/checklists?name=${checklistInput}&idCard=${cardId}&key=dc336cbd741eb8db35bfe2dd78eff829&token=a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));
        
        this.setState({cardChecklist: [...cardChecklist.concat({name:checklistInput,id:uuidv4()})]})
        this.setState({checklistInput:""})
    }


    addCheckList = () => {
        const { checkListInputShow, checklistInput } = this.state

        if (checkListInputShow) {
            return (
                <div className="main-checklist-card">
                    <div className="checklist-input">
                        <div className="checklist-add-header-card">
                            <p className="add-checklist-head">Add CheckList</p>
                            <button onClick={this.closeUserInput} className="add-card-close card-details-closebtn">
                                <CloseIcon className="close-checkin" />
                            </button>
                        </div>
                        <hr className="line" />
                        <input  onChange={this.newCheckList} value={checklistInput} placeholder="Enter" className="input-text" type="text" />
                        <br />
                        <button onClick={this.addCheckListInput} className="add-checklist-button2">Add</button>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }
    

    render() {
        const { cardChecklist} = this.state
        const { cardName ,cardId} = this.props

        return (
            <div className="popup-main-container">
                <div className="popup-datacard">
                    <div className="checklist-details-header">
                        <h2 className="card-heading">{cardName}</h2>
                        <button onClick={this.closePopup} className="add-card-close"><CloseIcon /></button>
                    </div>

                    <button onClick={this.showChecklistAdd} className="add-checklist-button">Add CheckList</button>
                    {this.addCheckList()}

                    <ul className="checkLists">
                        {cardChecklist.map(item => {
                            
                            return (
                                <li key={item.id}>
                                    <div>
                                        <h4>{item.name}</h4>
                                    </div>
                                    <hr />
                                    <CheckItems checklistId={item.id} cardId={cardId}/>
                                </li>
                            )
                        })}
                        
                    </ul>
                   
                </div>
            </div>
        )
    }
}

export default CardDetails