import { Component } from "react";
import * as TrelloData from "../../API/Api"
import List from "../Lists/List";
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';

import { v4 as uuidv4 } from 'uuid';

import "./boards.css"

class Boards extends Component {
    state = { boardsData: [], newListName: "", isError: false, enterListInputIsShow: false }



    async componentDidMount() {
        await TrelloData.getTrelloBoardsData()
            .then((trelloBoardsData) => {
                this.setState({ boardsData: trelloBoardsData.data, isError: false })

            }).catch((err) => {
                this.setState({ isError: true })
            })

    }

    onAddCardInput = (event) => {
        this.setState({ newListName: event.target.value })
    }

    addList = () => {
        const { newListName } = this.state
        fetch(`https://api.trello.com/1/lists?name=${newListName}&idBoard=633d0e9925c0f6019de8ea6e&key=dc336cbd741eb8db35bfe2dd78eff829&token=a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));

        this.setState(prevState => {
            return { boardsData: [...prevState.boardsData,{ name: newListName,id:uuidv4() }],newListName:"" }
        })
    }


    openCardTitleInput = () => {
        this.setState({ enterListInputIsShow: true })
    }

    closeCardTitleInput = () => {
        this.setState({ enterListInputIsShow: false })
    }

    displayListInputCard = () => {
        const { enterListInputIsShow, newListName } = this.state
        if (enterListInputIsShow) {
            return (
                <div className="list-input-card">
                    <input type="text" value={newListName} onChange={this.onAddCardInput} className="text-area-input" placeholder="Enter a list.." />
                    <div className="add-close-buttons">
                        <button onClick={this.addList} className="add-button-card">Add List</button>
                        <button onClick={this.closeCardTitleInput} className="add-card-close"><CloseIcon /></button>
                    </div>
                </div>
            )
        }
        return (
            <button onClick={this.openCardTitleInput} className="add-list-button">
                <AddIcon />
                Add a card
            </button>

        )
    }

    render() {
        const { boardsData } = this.state

        return (
            <div className="board-bgcard">
                <div className="all-lists-cards">
                    {boardsData.map((eachItem, i) => {
                        return <List key={i} eachList={eachItem} />
                    })}

                    <div>
                        {this.displayListInputCard()}
                    </div>
        
                </div>
            </div>
        )
    }
}


export default Boards