import { Component } from "react";
import CloseIcon from '@mui/icons-material/Close';

import Cards from "../Cards/cards"
import CardDetails from "../cardDetails/cardDetails"

import MoreHorizIcon from '@mui/icons-material/MoreHoriz';




import "./List.css"




class List extends Component {
    state = { isPopupDisplay: false, cardId: "", cardName: "", options: false }

    isPopupshows = (id, cardName) => {
        this.setState({ isPopupDisplay: true, cardId: id, cardName: cardName })
    }

    closePopup = () => {
        this.setState({ isPopupDisplay: false })
    }

    displayPopUp = () => {
        const { isPopupDisplay, cardId, cardName } = this.state
        if (isPopupDisplay) {
            return (
                <>
                    <CardDetails cardId={cardId} cardName={cardName} closePopup={this.closePopup} />
                </>
            )
        }
    }

    isShowOptions = () => {
        this.setState({ options: true })
    }

    closeOptionCard = () => {
        this.setState({ options: false })
    }

    deleteCard = async () => {
        const { id } = this.props.eachList
        fetch(`https://api.trello.com/1/lists/${id}/archiveAllCards?key=dc336cbd741eb8db35bfe2dd78eff829&token=a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));

    }


    displayOptionsList = () => {
        const { options } = this.state
        if (options) {
            return (
                <div className="options-main-card">
                    <div className="options-in-card">
                        <div className="option-card-header">
                            <p className="optin-heading">List Actions</p>
                            <button onClick={this.closeOptionCard} className="close-option-btn">
                                <CloseIcon />
                            </button>
                        </div>
                        <hr />
                        <ul className="options-list-card">
                            <li className="list-items">Add List...</li>
                            <li className="list-items">Copy List...</li>
                            <li className="list-items">Move List</li>
                            <li className="list-items">Watch</li>
                            <hr />
                            <li className="list-items">Automation</li>
                            <li className="list-items">When a card is added to the list…</li>
                            <li className="list-items">Every day, sort list by…</li>
                            <li className="list-items">Every Monday, sort list by…</li>
                            <li className="list-items">Create a custom rule</li>
                            <hr />
                            <button onClick={this.deleteCard} className="delete-option-btn">
                                <li >Archive all cards</li>
                            </button>
                        </ul>
                    </div>
                </div>
            )
        }
    }


    render() {
        const { eachList } = this.props
        

        const { name, id } = eachList
        return (
            <>
                <div className='list-container'>
                    <div className='list-card-header'>
                        <h4 className='list-card-heading'>{name}</h4>
                        <div onClick={this.isShowOptions}><MoreHorizIcon className='list-more-icon' /></div>
                    </div>
                    <div>
                        <Cards id={id} isPopupshows={this.isPopupshows} />
                    </div>
                    {this.displayOptionsList()}
                </div>

                {this.displayPopUp()}
            </>
        )


    }
}


export default List