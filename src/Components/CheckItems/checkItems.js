import { Component } from "react";

import * as TrelloData from "../../API/Api"
import { v4 as uuidv4 } from 'uuid';

import "./checkItems.css"

class CheckItems extends Component {
    state = { checklistData: [], addCheckItem: false, addCheckItemShow: false, checkItemuserInput: "" }

    async componentDidMount() {
        const { checklistId } = this.props
        await TrelloData.getTrelloCheckItems(checklistId)
            .then(checkItems => {
                this.setState({ checklistData: checkItems })
            })
    }

    CheckItemInput = (event) => {
        this.setState({ checkItemuserInput: event.target.value })
    }

    closeCheckItemInput = () => {
        this.setState({ addCheckItem: false })
    }

    onChangeItemInputStatus = () => {
        this.setState({ addCheckItem: true })
    }

    addCheckItemToData = () => {
        const { checkItemuserInput } = this.state
        const {checklistId} = this.props

        fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${checkItemuserInput}&key=dc336cbd741eb8db35bfe2dd78eff829&token=a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02`, {
            method: 'POST'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));

        this.setState({ checklistData: this.state.checklistData.concat({ name: checkItemuserInput,id:uuidv4() }) })
        console.log(this.state.checklistData)
        this.setState({ checkItemuserInput: "" })
    }

    showCheckItemInput = () => {
        const { addCheckItem ,checkItemuserInput} = this.state
        

        if (addCheckItem) {

            return (
                <div className="">
                    <textarea value={checkItemuserInput} onChange={this.CheckItemInput} className="item-input-text" placeholder="Enter CheckItem"></textarea>
                    <br />
                    <button onClick={this.addCheckItemToData} className="checkItem-Add-btn">Add</button>
                    <button onClick={this.closeCheckItemInput} className="checkItem-close-btn">Close</button>
                </div>
            )
        } else {
            return (
                <button onClick={this.onChangeItemInputStatus} className="add-checkItem-button">Add Item</button>
            )
        }
    }

    checkEl = (id) => {
        const {checklistData} = this.state
        
        
        let value 

        const newData = checklistData.map(item =>{
            
            if(item.id === id){
                if(item.state === "complete"){
                    value = "incomplete"
                    return {...item, state: "incomplete"}
                    
                }else{
                    value = "complete"
                    return{...item,state:"complete"}
                    
                }
            }
            return item
        })

        this.setState({checklistData:newData})
        
        const { cardId } = this.props
        fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${id}?state=${value}&key=dc336cbd741eb8db35bfe2dd78eff829&token=a67df0fb848c379fba9148cd2cbbc7147278af0979962c2e92211ed1ffaaea02`, {
            method: 'PUT'
        })
            .then(response => {
                console.log(
                    `Response: ${response.status} ${response.statusText}`
                );
                return response.text();
            })
            .then(text => console.log(text))
            .catch(err => console.error(err));
    }




    render() {
        const { checklistData } = this.state
        console.log(checklistData)
        return (
            <div>
                {checklistData.map(eachItem => {

                    let checklistCheck = eachItem.state === "complete" ? 'isChecked' : ""
                    let isChecked = eachItem.state === "complete" ? 'checked' : ""

                    return (
                        <div key={eachItem.id} className="checkItems">
                            <input defaultChecked={isChecked} id={eachItem.id} onClick={() => this.checkEl(eachItem.id)} type="checkbox" className="checkitem-checkbox" />
                            <label htmlFor={eachItem.id} className={`checkitem-text ${checklistCheck}`}>{eachItem.name}</label>
                        </div>)
                })}
                {this.showCheckItemInput()}
            </div>
        )
    }
}

export default CheckItems