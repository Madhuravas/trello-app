
import Navbar from './Components/Navbar/navbar';
import Boards from './Components/Boards/boards';
// import CardDetails from './Components/cardDetails/cardDetails';

import './App.css';

function App() {
  return (
    <>
    <Navbar/>
    <Boards/>
    
    </>
  );
}

export default App;
